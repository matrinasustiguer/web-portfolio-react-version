import React from 'react'
import me from '../assets/media/me.png'

export default function Footer() {
  return (
    <footer className='foot'>
      <img alt='logo' className='footer-pic' src={me} />
      <p>Web Developer Portfolio by Trina Sustiguer 2021</p>
    </footer>
  )
}
