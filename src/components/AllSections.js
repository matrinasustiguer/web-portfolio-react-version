import React from 'react'
import about from '../assets/media/about.png'
import skills from '../assets/media/skills.png'
import projects from '../assets/media/projects.png'
import contact from '../assets/media/contact.png'
import dev from '../assets/media/dev.png'
import ful from '../assets/media/ful.png'
import stac from '../assets/media/stac.png'
import me from '../assets/media/me.png'
import HyperT from '../assets/media/HyperT.png'
import Casc from '../assets/media/Casc.png'
import Ja from '../assets/media/Ja.png'
import Boot from '../assets/media/Boot.png'
import P from '../assets/media/P.png'
import Lar from '../assets/media/Lar.png'
import MS from '../assets/media/MS.png'
import He from '../assets/media/He.png'
import Sub from '../assets/media/Sub.png'
import Vis from '../assets/media/Vis.png'
import G from '../assets/media/G.png'
import Sa from '../assets/media/Sa.png'
import Mong from '../assets/media/Mong.png'
import Reac from '../assets/media/Reac.png'
import Nod from '../assets/media/Nod.png'
import Redu from '../assets/media/Redu.png'
import Things from '../assets/media/Things.png'
import BC from '../assets/media/BC.png'
import Presters from '../assets/media/Presters.png'
import Screen from '../assets/media/Screen.png'
import Rent from '../assets/media/Rent.jpg'

export default function AllSections() {
  return (
    <>
      <div className='all-sections'>
        <div className='landing-area' id='landing'>
          <div className='main-cogs' id='cogs'>
            <img alt='pic' className='gear cog1' src={about} />
            <img alt='pic' className='gear cog2' src={skills} />
            <img alt='pic' className='gear cog3' src={projects} />
            <img alt='pic' className='gear cog4' src={contact} />
          </div>
          <div className='main-landing'>
            <div className='dev'>
              <div className='main-dev'>
                <img alt='pic' src={dev} />
              </div>
            </div>
            <div className='stack'>
              <img alt='pic' src={ful} />
              <img alt='pic' src={stac} />
            </div>
          </div>
        </div>
        <div className='about-section' id='about'>
          <div className='box'>
            <div className='blocks-1' data-aos='fade-up'>
              <img alt='pic' className='pic' src={me} />
            </div>
            <div data-aos='fade-down' className='content'>
              <h1 className='heading-1'>About Me</h1>
              <p>
                Hi! I'm a full stack web developer. My name is Trina Sustiguer.
                I hope you're having a good day!
              </p>
              <p>
                I like building things and seeing how they work. Learning is
                something I really love and enjoy. Its many forms are limitless
                and I like enhancing my current knowledge and skills. Indeed, it
                kind of makes you feel everything is possible! I simply believe
                in continuous learning and innovation.
              </p>
              <p>
                <em>Coding is my passion.</em>
              </p>
              <p>I'm also an aspiring polyglot.</p>
            </div>
          </div>
        </div>
        <div className='skills-section' id='skills'>
          <div className='box-c'>
            <div data-aos='flip-right'>
              <h1 className='heading-2'>Skills</h1>
            </div>
            <div data-aos='fade-up' className='griddy'>
              <img alt='pic' className='logos' src={HyperT} />
              <img alt='pic' className='logos' src={Casc} />
              <img alt='pic' className='logos' src={Ja} />
              <img alt='pic' className='logos' src={Boot} />
              <img alt='pic' className='logos' src={P} />
              <img alt='pic' className='logos' src={Lar} />
              <img alt='pic' className='logos' src={MS} />
              <img alt='pic' className='logos' src={He} />
              <img alt='pic' className='logos' src={Sub} />
              <img alt='pic' className='logos' src={Vis} />
              <img alt='pic' className='logos' src={G} />
              <img alt='pic' className='logos' src={Sa} />
              <img alt='pic' className='logos' src={Reac} />
              <img alt='pic' className='logos' src={Mong} />
              <img alt='pic' className='logos' src={Nod} />
              <img alt='pic' className='logos' src={Redu} />
            </div>
          </div>
        </div>
        <div className='projects-section' id='projects'>
          <div className='box-c'>
            <div className='blocks' data-aos='zoom-in'>
              <h1 className='heading-3'>Projects</h1>
            </div>
            <div className='selections'>
              <div data-aos='fade-right' className='projects'>
                <a
                  className='brand'
                  href='http://thingsonlineshop.herokuapp.com/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <img alt='pic' className='project' src={Things} />
                </a>
                <div className='description'>
                  <h2>Things Online Shop</h2>
                  <p>
                    <a
                      className='visit'
                      href='http://thingsonlineshop.herokuapp.com/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Live
                    </a>
                    <a
                      className='visit'
                      href='https://gitlab.com/matrinasustiguer/ecommerce-app-things'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Codes
                    </a>
                  </p>
                  <p>
                    Ecommerce web app for a fictional online shop called Things.
                    Made with MongoDB, Express, React, Node, and Redux. You may
                    contact me for admin access.
                  </p>
                  <div className='tags'>
                    <img alt='pic' className='tag' src={Reac} />
                    <img alt='pic' className='tag' src={Mong} />
                    <img alt='pic' className='tag' src={Nod} />
                    <img alt='pic' className='tag' src={Redu} />
                    <img alt='pic' className='tag' src={He} />
                  </div>
                </div>
              </div>
              <div data-aos='fade-right' className='projects'>
                <a
                  className='brand-2'
                  href='https://matrinasustiguer.gitlab.io/caps-one/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <img alt='pic' className='project' src={BC} />
                </a>
                <div className='description'>
                  <h2>Backyard Carnival</h2>
                  <p>
                    <a
                      className='visit'
                      href='https://matrinasustiguer.gitlab.io/caps-one/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Live
                    </a>
                    <a
                      className='visit'
                      href='https://gitlab.com/matrinasustiguer/caps-one'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Codes
                    </a>
                  </p>
                  <p>
                    A static website about a rental service provider of party
                    equipments. Written in pure CSS3 and HTML5.
                  </p>
                  <div className='tags'>
                    <img alt='pic' className='tag' src={HyperT} />
                    <img alt='pic' className='tag' src={Casc} />
                  </div>
                </div>
              </div>
              <div data-aos='fade-right' className='projects'>
                <a
                  className='brand'
                  href='http://safe-dusk-16845.herokuapp.com/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <img alt='pic' className='project' src={Presters} />
                </a>
                <div className='description'>
                  <h2>Prester's</h2>
                  <p>
                    <a
                      className='visit'
                      href='http://safe-dusk-16845.herokuapp.com/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Live
                    </a>
                    <a
                      className='visit'
                      href='https://gitlab.com/matrinasustiguer/capstone-2'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Codes
                    </a>
                  </p>
                  <p>
                    Dynamic website. A visit booking system web app for a
                    fictional pet rescue and shelter society. You may contact me
                    for admin access.
                  </p>
                  <div className='tags'>
                    <img alt='pic' className='tag' src={Lar} />
                    <img alt='pic' className='tag' src={MS} />
                    <img alt='pic' className='tag' src={Boot} />
                    <img alt='pic' className='tag' src={Ja} />
                    <img alt='pic' className='tag' src={P} />
                    <img alt='pic' className='tag' src={He} />
                  </div>
                </div>
              </div>
              <div data-aos='fade-right' className='projects'>
                <a
                  className='brand-2'
                  href='https://cov19-screenme.herokuapp.com/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <img alt='pic' className='project' src={Screen} />
                </a>
                <div className='description'>
                  <h2>Cov19 ScreenMe</h2>
                  <p>
                    <a
                      className='visit'
                      href='https://cov19-screenme.herokuapp.com/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Live
                    </a>
                    <a
                      className='visit'
                      href='https://gitlab.com/MarkJay/reactcapstonemaking'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Codes
                    </a>
                  </p>
                  <p>
                    A simplified COVID-19 Screening Tool based on DOH
                    (Department of Health) Algorithm. Made as a part of a
                    3-person team.
                  </p>
                  <div className='tags'>
                    <img alt='pic' className='tag' src={Reac} />
                    <img alt='pic' className='tag' src={Boot} />
                    <img alt='pic' className='tag' src={He} />
                    <img alt='pic' className='tag' src={Vis} />
                  </div>
                </div>
              </div>
              <div data-aos='fade-right' className='projects'>
                <a
                  className='brand'
                  href='https://rolandolagan.gitlab.io/team-trilogy-minijs/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <img alt='pic' className='project' src={Rent} />
                </a>
                <div className='description'>
                  <h2>Renta Bike</h2>
                  <p>
                    <a
                      className='visit'
                      href='https://rolandolagan.gitlab.io/team-trilogy-minijs/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Live
                    </a>
                    <a
                      className='visit'
                      href='https://gitlab.com/rolandolagan/team-trilogy-minijs'
                      target='_blank'
                      rel='noreferrer'
                    >
                      View Codes
                    </a>
                  </p>
                  <p>
                    A simple website app for a fictional bike rental service
                    provider written with HTML, CSS, Bootstrap, and JavaScript.
                    Made as a part of a 3-person team.
                  </p>
                  <div className='tags'>
                    <img alt='pic' className='tag' src={Ja} />
                    <img alt='pic' className='tag' src={Boot} />
                    <img alt='pic' className='tag' src={HyperT} />
                    <img alt='pic' className='tag' src={Casc} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='contact-section' id='contact'>
          <h1 className='heading-4'>Contact</h1>
          <div className='contact-box'>
            <div className='contact-left'>
              <p>Thanks for visiting!</p>
              <p>
                You may contact me at{' '}
                <a className='email' href='mailto:sustiguermatrina@gmail.com'>
                  sustiguermatrina@gmail.com
                </a>
              </p>
            </div>
            <div className='contact-right'>
              <p>
                <a
                  href='https://gitlab.com/matrinasustiguer/web-portfolio-react-version'
                  target='_blank'
                  rel='noreferrer'
                  className='contact-link'
                >
                  Portfolio Source Code <i className='fas fa-code icon'></i>
                </a>
              </p>
              <p>
                <a
                  href='https://gitlab.com/matrinasustiguer'
                  target='_blank'
                  rel='noreferrer'
                  className='contact-link'
                >
                  Gitlab <i className='fab fa-gitlab icon'></i>
                </a>
              </p>
              <p>
                <a
                  href='https://www.linkedin.com/in/ma-trina-sustiguer'
                  target='_blank'
                  rel='noreferrer'
                  className='contact-link'
                >
                  LinkedIn <i className='fab fa-linkedin favi icon'></i>
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
