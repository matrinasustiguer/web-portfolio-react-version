import React from 'react'
import '../index.css'
import projects from '../assets/media/projects.png'

export default function AppNavBar(){
    return(
        <div className="nav-section">
            <div className="nav-left">
                <a href="#">
                    <img alt="pic" className="landing-click" src={projects}/>
                </a>
            </div>
            <div className="nav-right">
                <a href="#about" className="nav-clicks" rel="noreferrer">About</a>
                <a href="#skills" className="nav-clicks" rel="noreferrer">Skills</a>
                <a href="#projects" className="nav-clicks" rel="noreferrer">Projects</a>
                <a href="#contact" className="nav-clicks" rel="noreferrer">Contact</a>
            </div>
        </div>
    )
}