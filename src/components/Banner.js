import React from 'react'
import {Jumbotron} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import '../index.css'

export default function Banner({data}){
	console.log(data.title)
	console.log(data.content)
	console.log(data.destination)
	return(
		<>
		<Jumbotron>
			<h1>{data.title}</h1>
			<p>{data.content}</p>
		
			<Link to={data.destination}>{data.label}</Link>
			
		</Jumbotron>
		</>
	)
}