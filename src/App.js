import React from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import './index.css'
import LandingPage from './pages/LandingPage'
import ErrorPage from './pages/Error'

function App(){
  return(
    <>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LandingPage}/>
          <Route component={ErrorPage}/>
        </Switch>
      </BrowserRouter>
      <script defer>{AOS.init({duration: 1200})}</script>
    </>
    )
}

export default App