import React from 'react'
import Banner from '../components/Banner'

export default function ErrorPage(){
	const data = {
		title: "404 | Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}
	return(
		<Banner data={data}/>
	)
}