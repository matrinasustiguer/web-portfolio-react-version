import React from 'react';
import AppNavBar from '../components/AppNavBar';
import AllSections from '../components/AllSections';
import Footer from '../components/Footer';
import '../index.css'

export default function LandingPage() {
    return (
        <>
            <AppNavBar/>
            <AllSections/>
            <Footer/>
        </>
    )
}